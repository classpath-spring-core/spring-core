# Spring Core

## Project Setup
1. clone the repository
```java
git clone git@gitlab.com:classpath-spring-core/spring-core.git
```
2. Checkout to the tag

```java
git checkout 1.0-scaffolding-code
```

## Spring Core

### Spring Framework consists of 
- Dependency Injection (IOC)
- Aspect Oriented Programming (AOP)


## Spring IOC
- Inversion of Control
- Inject dependencies for the application
- Promotes loose coupling between classes
- Can be implemented using XML or Annotations

## Project setup
1. Create a Maven project
2. Add the Spring dependency
```xml
    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <spring.version>5.2.3.RELEASE</spring.version>
    </properties>
    <dependencies>
       <dependency>
           <groupId>org.springframework</groupId>
           <artifactId>spring-context</artifactId>
           <version>${spring.version}</version>
       </dependency>
    </dependencies>
```
3. Create ``application-context.xml`` file under ``src/main/resources`` directory
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!-- constructor dependency injection -->
    <bean id="itemService" class="com.classpath.spring.core.service.ItemServiceImpl">
        <constructor-arg name="itemDAO" ref="itemDAO" />
    </bean>

    <bean id="itemDAO" class="com.classpath.spring.core.dao.ItemDAOImpl"/>
</beans>
```
4. In the ``Client`` class load the ``ApplicationContext``
```java
public class Client {
    public static void main(String[] args) {
            ApplicationContext applicationContext =
                    new ClassPathXmlApplicationContext("application-context.xml");
            ItemService itemService = applicationContext.getBean("itemService", ItemService.class);
            Item item = new Item("IPhone-11", 1_00_000);
            itemService.saveItem(item);
        }
    }
```

5. Setting the dependency using ``property``
```xml
 <!-- dependency injection using property-->
    <bean id="itemDAO" class="com.classpath.spring.core.dao.ItemDAOImpl">
        <property name="connectStr" value="jdbc:mysql://localhost:3306/emp_db" />
        <property name="username" value="test"/>
        <property name="password" value="pass" />
    </bean>
```

### Constructor DI vs Property DI

- Use constructor DI for all mandatory properties
- The constructor should not contain more then 4-5 arguments - Guideline
- Use property DI for all optional properties

## Injecting Collections

In the `application-context.xml` 
```xml
<!-- dependency injection using property-->
    <bean id="itemDAO" class="com.classpath.spring.core.dao.ItemDAOImpl">
        <property name="connectStr" value="jdbc:mysql://localhost:3306/emp_db" />
        <property name="username" value="test"/>
        <property name="password" value="pass" />

        <!-- Setting the collection as dependency-->
        <!-- For simple type use value -->
        <!-- For complex type use ref -->
        <property name="items">
            <set>
                <ref bean="I-Phone-11"/>
                <ref bean="laptop"/>
            </set>
        </property>
    </bean>

    <bean id="laptop" class="com.classpath.spring.core.model.Item">
        <constructor-arg name="name" value="Thinkpad-T580"/>
        <constructor-arg name="price" value="85000"/>
    </bean>

    <bean id="I-Phone-11" class="com.classpath.spring.core.model.Item">
        <constructor-arg name="name" value="I-Phone-11"/>
        <constructor-arg name="price" value="120000"/>
    </bean>

    <bean id="Macbook-pro" class="com.classpath.spring.core.model.Item">
        <constructor-arg name="name" value="Macbook-Pro"/>
        <constructor-arg name="price" value="225000"/>
    </bean>
```
In case of `list` replace `set` with `list`
[Reference link for Injecting Collection](https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/core.html#beans-collection-elements)

In the `Client` class

```java
    Set<Item> items = itemService.fetchAllItems();

    items.forEach(i -> System.out.printf("Item Name: %s, Item Price: %f %n", i.getName(), i.getPrice()));
```

## Externalizing properties 
1. Load the `PropertySourcesPlaceholderConfigurer` bean
```xml
  <!-- Externalizing the properties to a separate file -->
    <bean class="org.springframework.context.support.PropertySourcesPlaceholderConfigurer">
        <property name="location" value="classpath:jdbc.properties"/>
    </bean>
```

2. Externalize the properties inside `resources` directory in a file called `jdbc.properties`
```properties
mysql.emp_db.connectStr=jdbc:mysql://localhost:3306/emp_db
mysql.emp_db.username=root
mysql.emp_db.password=welcome
```

3. Inject the properties inside the bean definition
```xml
    <!-- dependency injection using property-->
    <!-- Injecting properties through external file-->
    <bean id="itemDAO" class="com.classpath.spring.core.dao.ItemDAOImpl">
        <property name="connectStr" value="${mysql.emp_db.connectStr}" />
        <property name="username" value="${mysql.emp_db.username}"/>
        <property name="password" value="${mysql.emp_db.password}" />
        ...
     </bean> 
```
[Injecting external properties](https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/core.html#beans-value-annotations)


## Bean Scope
There are 4 scopes available for beans
1. Singleton - default
2. Prototype
3. Request - Available when running on servlet container
4. Session - Available when running on servlet container
5. Application - Available when running on servlet container

### Singleton
- Only one instance will be available inside the JVM
- This is the default scope

### Prototype
- New instance will be created when `getBean` method is invoked 
- `scope=prototype` should be defined in the `bean` tag

## Mapping Inheritance

- set the `abstract` property to true in the parent bean definition
```xml
  <!-- set the abstract property to true-->
    <bean id="item" class="com.classpath.spring.core.model.Item" abstract="true"/>
```
- In the child class bean definition add the `parent` attribute and refer to the parent bean definition
```xml
 <!-- set the parent to the bean id (item)-->
    <bean id="laptop"
          class="com.classpath.spring.core.model.ElectronicItem"
          parent="item">

        <constructor-arg name="name" value="Thinkpad-T580"/>
        <constructor-arg name="price" value="85000"/>
        <constructor-arg name="version" value="1.0.0"/>
    </bean>
```
In the Child class
```java
public class ElectronicItem extends Item {...} 
```
```java
public class ElectricItem extends Item{...} 
```

In the `Client` class
```java
    // Mapping Inheritance
    Item iphone11_1 = applicationContext.getBean("I-Phone-11", Item.class);
    ElectronicItem mackbookPro = applicationContext.getBean("Macbook-pro", ElectronicItem.class);
    System.out.println(" IPhone - 11: "+ iphone11_1);
    System.out.println(" Macbook Pro: "+ mackbookPro);
```
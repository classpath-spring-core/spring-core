package com.classpath.spring.core.dao;

import com.classpath.spring.core.model.Item;

import java.util.List;
import java.util.Set;

public class ItemDAOImpl implements ItemDAO {

    private String connectStr;
    private String username;
    private String password;
    private Set<Item> items;

    protected  ItemDAOImpl(){}

    public ItemDAOImpl(String connectStr, String username, String password) {
        this.connectStr = connectStr;
        this.username = username;
        this.password = password;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public String getConnectStr() {
        return connectStr;
    }

    public void setConnectStr(String connectStr) {
        this.connectStr = connectStr;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



    @Override
    public Item saveItem(Item item) {
        System.out.println("Inside the save Item method of ItemDAOImpl class");
        System.out.printf("Connect string is %s with username as: %s and password as: %s %n", this.connectStr, this.username, this.password);
        this.items.add(item);
        return item;
    }

    @Override
    public Set<Item> fetchItems() {
        return this.items;
    }

    @Override
    public Item findItemById(long itemId) {
        return null;
    }

    @Override
    public void deleteItem(long itemId) {

    }
}
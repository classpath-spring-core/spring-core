package com.classpath.spring.core.dao;

import com.classpath.spring.core.model.Item;

import java.util.Set;

public interface ItemDAO {

    Item saveItem(Item item);

    Set<Item> fetchItems();

    Item findItemById(long itemId);

    void deleteItem(long itemId);

}
package com.classpath.spring.core.client;

import com.classpath.spring.core.model.ElectronicItem;
import com.classpath.spring.core.model.Item;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {
    public static void main(String[] args) {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("application-context.xml");
        /*
            Item item = new Item("IPhone-11", 1_00_000);
            itemService.saveItem(item);
        */
        /*
            Set<Item> items = itemService.fetchAllItems();
            items.forEach(i ->
                System.out.printf("Item Name: %s, Item Price: %f %n", i.getName(), i.getPrice()));
        */
        //Scope - Singleton
        /*
        ItemService itemService = applicationContext.getBean("itemService", ItemService.class);
        ItemService itemService2 = applicationContext.getBean("itemService", ItemService.class);
        System.out.printf("Beans with scope as singleton will point to the same instance in the JVM: %s %n",itemService == itemService2);

        //Scope - Prototype
        Item iphone11_1 = applicationContext.getBean("I-Phone-11", Item.class);
        Item iphone11_2 = applicationContext.getBean("I-Phone-11", Item.class);
        System.out.printf("Beans with scope as prototype will refer to different instance in the JVM: %s %n",iphone11_1 == iphone11_2);
        */

        // Mapping Inheritance
        Item iphone11_1 = applicationContext.getBean("I-Phone-11", Item.class);
        ElectronicItem mackbookPro = applicationContext.getBean("Macbook-pro", ElectronicItem.class);

        System.out.println(" IPhone - 11: "+ iphone11_1);
        System.out.println(" Macbook Pro: "+ mackbookPro);

    }
}
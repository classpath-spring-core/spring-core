package com.classpath.spring.core.model;


import java.util.Objects;

public class ElectronicItem extends Item {

    private String version;

    public ElectronicItem(String name, double price, String version){
        super(name, price);
        this.version = version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ElectronicItem that = (ElectronicItem) o;
        return Objects.equals(version, that.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), version);
    }

    @Override
    public String toString() {
        return "ElectronicItem{" +
                "version='" + version + '\'' +
                '}';
    }
}
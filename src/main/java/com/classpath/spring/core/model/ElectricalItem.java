package com.classpath.spring.core.model;

import java.util.Objects;

public class ElectricalItem extends Item {

    private int wattage;

    public ElectricalItem(String name, double price, int watts ){
        super(name, price);
        this.wattage = watts;
    }

    public int getWattage() {
        return wattage;
    }

    public void setWattage(int wattage) {
        this.wattage = wattage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ElectricalItem that = (ElectricalItem) o;
        return wattage == that.wattage;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), wattage);
    }

    @Override
    public String toString() {
        return "ElectricalItem{" +
                "wattage=" + wattage +
                '}';
    }
}
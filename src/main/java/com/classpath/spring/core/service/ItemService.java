package com.classpath.spring.core.service;

import com.classpath.spring.core.model.Item;

import java.util.Set;

public interface ItemService {

    Item saveItem(Item item);

    Set<Item> fetchAllItems();

    Item fetchItemById(long itemId);

    void deleteItem(long itemId);
}
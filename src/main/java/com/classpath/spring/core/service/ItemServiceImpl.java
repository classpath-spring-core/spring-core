package com.classpath.spring.core.service;

import com.classpath.spring.core.dao.ItemDAO;
import com.classpath.spring.core.model.Item;

import java.util.Set;

public class ItemServiceImpl implements ItemService {

    private final ItemDAO itemDAO;

    public ItemServiceImpl(ItemDAO itemDAO){
        this.itemDAO = itemDAO;
    }

    @Override
    public Item saveItem(Item item) {
        return this.itemDAO.saveItem(item);
    }

    @Override
    public Set<Item> fetchAllItems() {
        return this.itemDAO.fetchItems();
    }

    @Override
    public Item fetchItemById(long itemId) {
        return this.itemDAO.findItemById(itemId);
    }

    @Override
    public void deleteItem(long itemId) {
        this.itemDAO.deleteItem(itemId);
    }
}